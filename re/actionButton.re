[@bs.deriving abstract]
type jsProps = {
  buttonColor: string,
  title: string
};

module Item = {
  [@bs.module "react-native-action-button"] [@bs.scope "default"]
  external actionButtonItem : ReasonReact.reactClass =
    "Item";
  let make = (~buttonColor="rgba(0,0,0,1)", ~title="", children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass=actionButtonItem,
      ~props=jsProps(~buttonColor, ~title),
      children
    );
};

module Icon = {
  [@bs.deriving abstract]
  type iconProps = {
    style: option(BsReactNative.Style.t),
    name: string
  };
  [@bs.module "react-native-vector-icons/Ionicons"] external icon : ReasonReact.reactClass =
    "default";
  let make = (~name="", ~style=?, children) =>
    ReasonReact.wrapJsForReason(~reactClass=icon, ~props=iconProps(~name, ~style), children);
};

[@bs.module "react-native-action-button"] external actionButton : ReasonReact.reactClass =
  "default";

let make = (~buttonColor="rgba(0,0,0,1)", ~title="", children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass=actionButton,
    ~props=jsProps(~buttonColor, ~title),
    children
  );
