open BsReactNative;

module Styles = {
  let container = Style.(style([flex(1.), justifyContent(Center), alignItems(Center)]));
  let row = Style.(style([flexDirection(Row)]));
  let input = Style.(style([flexGrow(1.)]));
  let actionButtonIcon =
    Style.(style([fontSize(Float(20.)), height(Pt(22.)), color(String("white"))]));
};

type item = {
  text: string,
  complete: bool
};

type state = {
  items: array(item),
  input: string
};

type action =
  | Add
  | NewItemEdit(string);

let component = ReasonReact.reducerComponent("Todo");

let make = (_children) => {
  ...component,
  initialState: () => {input: "", items: [||]},
  reducer: (action, state: state) =>
    switch action {
    | Add =>
      ReasonReact.Update({
        input: "",
        items: Array.append(state.items, [|{text: state.input, complete: false}|])
      })
    | NewItemEdit(text) => ReasonReact.Update({...state, input: text})
    },
  render: (_self) =>
    <View style=Styles.container>
      <Text> (ReasonReact.string("Foo")) </Text>
      <ActionButton buttonColor="rgba(231, 76, 60, 1)">
        <ActionButton.Item buttonColor="#9b59b6" title="New Task">
          <ActionButton.Icon name="md-create" style=Styles.actionButtonIcon />
        </ActionButton.Item>
      </ActionButton>
    </View>
};
